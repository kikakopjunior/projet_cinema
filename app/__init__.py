from flask import Flask
from dotenv import load_dotenv
import os

load_dotenv()
app = Flask(__name__)
HOST = os.getenv('HOST')
PORT = os.getenv('PORT')
ID_SHEETS = os.getenv('ID_SHEETS')

app.config['HOST'] = HOST
app.config['PORT'] = PORT
app.config['ID_SHEETS'] = ID_SHEETS
from app import views

from app import errors
