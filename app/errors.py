from app import app
from flask import render_template, flash, redirect, url_for, jsonify, request


@app.errorhandler(400)
def bad_request_error(error):
    return jsonify({'error': str(error)}), 400


@app.errorhandler(401)
def unauthorized_error(error):
    return jsonify({'error': str(error)}), 401


@app.errorhandler(403)
def forbidden_error(error):
    return jsonify({'error': str(error)}), 403


@app.errorhandler(404)
def not_found_error(error):
    return jsonify({'error': str(error)}), 404


@app.errorhandler(405)
def method_not_allowed_error(error):
    return jsonify({'error': str(error)}), 405


@app.errorhandler(500)
def internal_error(error):
    return jsonify({'error': str(error)}), 500


@app.errorhandler(Exception)
def unhandled_exception(error):
    app.logger.error(f'Unhandled Exception: {error}')
    return jsonify({'error': 'Internal Server Error'}), 500
