import logging
import gspread
import os
from oauth2client.service_account import ServiceAccountCredentials
from datetime import datetime, timedelta

# Configuration du logger
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

def get_key_json() -> str:
    path = os.path.join(os.path.dirname(__file__), 'key.json')
    return path

class SheetsGoogle:
    def __init__(self, key_json: str) -> None:
        self.key_json = key_json
        self.scopes = ['https://www.googleapis.com/auth/spreadsheets',
                       'https://www.googleapis.com/auth/drive']
        try:
            self.credentials = ServiceAccountCredentials.from_json_keyfile_name(
                self.key_json, self.scopes)
            self.gc = gspread.authorize(self.credentials)
        except Exception as e:
            logger.error(
                f"Erreur lors de la configuration de l'API Google Sheets : {e}")

    def get_sheet_by_id(self, sheet_id: str) -> list:
        try:
            gc = self.gc.open_by_key(sheet_id).sheet1
            data = gc.get_all_records()
            filtered_data = [record for record in data if any(
                value.strip() for value in record.values())]
            return filtered_data
        except gspread.exceptions.SpreadsheetNotFound:
            logger.error("Feuille de calcul introuvable.")
            return []
        except gspread.exceptions.APIError as e:
            logger.error(f"Erreur API : {e}")
            return []
        except Exception as e:
            logger.error(f"Erreur inattendue : {e}")
            return []

    def get_sheet_cell(self, sheet_id: str):
        try:
            worksheet = self.gc.open_by_key(sheet_id).sheet1

            # Obtenez le nombre total de lignes et de colonnes
            row_count = worksheet.row_count
            col_count = worksheet.col_count

            # Construisez la chaîne de plage
            # Par exemple, si col_count est 26 (colonne Z) et row_count est 100
            range_str = f'A1:{chr(64 + col_count)}{row_count}'

            # Sélectionnez toutes les cellules dans cette plage
            data = worksheet.range(range_str)
            return data
        except gspread.exceptions.SpreadsheetNotFound:
            logger.error("Feuille de calcul introuvable.")
            return []
        except gspread.exceptions.APIError as e:
            logger.error(f"Erreur API : {e}")
            return []
        except Exception as e:
            logger.error(f"Erreur inattendue : {e}")
            return []

class SheetsAnalyze:
    def __init__(self, data) -> None:
        self.data = data

    def filter_data_result(self) -> dict:
        try:
            option_1 = 0
            option_2 = 0
            len_data = len(self.data)
            for dict_sub in self.data:
                horodateur = dict_sub.get('Horodateur', '')
                hello = dict_sub.get('HELLO', '')
                if hello == 'Option n° 1':
                    option_1 += 1
                elif hello == 'Option 2':
                    option_2 += 1

            if option_1 > option_2:
                result = 'option_1'
            elif option_1 < option_2:
                result = 'option_2'
            else:
                result = 'default'

            return {
                'option_1': option_1,
                'option_2': option_2,
                'len_data': len_data,
                'result': result
            }
        except KeyError as e:
            logger.warning(f"Clé manquante dans les données : {e}")
            return {'error': 'Invalid data format'}
        except Exception as e:
            logger.error(f"Erreur inattendue : {e}")
            return {'error': 'Internal error'}

    def __start_current_day(self, delay_fetch: int) -> tuple:
            now = datetime.now()
            delay_fetch_result = now - timedelta(minutes=delay_fetch)
            return now, delay_fetch_result

    def filter_start_session(self, data: list) -> list:
        filtered_data = []
        now, delay_fetch_result = self.__start_current_day(2)
        for dict_sub in data:
            horodateur = dict_sub.get('Horodateur', '')
            value_answer = dict_sub.get('Votre décision :', '')

            if horodateur and value_answer:
                try:
                    date_obj = datetime.strptime(horodateur, '%d/%m/%Y %H:%M:%S')
                    if delay_fetch_result <= date_obj <= now:
                        filtered_data.append(dict_sub)
                except ValueError:
                    # Gérer ou enregistrer l'erreur de format de date
                    pass
        return filtered_data

    def filter_result_session(self, data:list) -> dict:
        option_1 = 0
        option_2 = 0
        count = 0
        try:
            for dict_sub in data:
                horodateur = dict_sub.get('Horodateur', '')
                value_answer = dict_sub.get('Votre décision :', '')
                if horodateur and value_answer:
                    if value_answer == "Vous choisissez d'envoyer une vielle connaissance de Dereck." :
                        option_1 += 1
                    elif value_answer == "Vous choisissez de laisser Dereck tremper dans ses sales histoires, parfois ça a du bon..":
                        option_2 += 1
                    count += 1
            if option_1 > option_2:
                result = 'option_1'
            elif option_1 < option_2:
                result = 'option_2'
            else:
                result = 'default'

            return {
                'option_1': option_1,
                'option_2': option_2,
                'count': count,
                'result': result
            }

        except KeyError as e:
            logger.warning(f"Clé manquante dans les données : {e}")
            return {'error': 'Invalid data format'}
        except Exception as e:
            logger.error(f"Erreur inattendue : {e}")
            return {'error': 'Internal error'}
