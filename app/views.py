from app import app
from flask import render_template, jsonify, request, url_for
from app.sheets import get_key_json, SheetsGoogle, SheetsAnalyze
from app.config import ID_SHEETS

@app.route('/')
def index():
    video_urls = [
        url_for('static', filename='img/video1.mp4'),
        url_for('static', filename='img/video2.mp4'),
        url_for('static', filename='img/video3.mp4')
    ]
    host = request.host
    host_url = request.host_url
    return render_template('index.html', title='Home', video_urls=video_urls, api_urls=host_url, host=host)

@app.route('/api/v1/temperature', methods=['GET'])
def get_temperature():
    return jsonify({'temperature': 22})

@app.route('/api/v1/ping', methods=['GET'])
def ping():
    return jsonify({'status': 'ok'})

@app.route('/api/v1/sheets_start', methods=['GET'])
def start_sheets_capture():
    try:
        sheet = SheetsGoogle(get_key_json())
        data = sheet.get_sheet_by_id(ID_SHEETS)
        if data:
            analyze = SheetsAnalyze(data)
            data = analyze.filter_start_session(data)
            start_session_data = analyze.filter_result_session(data)
            return jsonify({'data': start_session_data})
        else:
            return jsonify({'error': 'Aucune donnée trouvée'}), 404
    except Exception as e:
        app.logger.error(f"Erreur lors de l'accès aux données des feuilles de calcul : {e}")
        return jsonify({'error': 'Erreur interne du serveur'}), 500