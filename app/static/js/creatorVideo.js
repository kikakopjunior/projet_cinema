export class CreatorVideo {
  constructor(pathVideo) {
    this.pathVideo = pathVideo;
    this.containerElement = this.createContainer();
    this.videoElement = this.createVideo();
  }
  createContainer() {
    let containerElement = document.createElement("div");
    containerElement.setAttribute("class", "container_video");
    return containerElement;
  }
  createVideo() {
    let videoElement = document.createElement("video");
    let sourceElement = document.createElement("source");

    videoElement.controls = true;
    videoElement.className = "video_controllers";

    sourceElement.src = this.pathVideo;
    sourceElement.type = "video/mp4";

    videoElement.appendChild(sourceElement);
    return videoElement;
  }

  switchVideo(pathVideo) {
    this.pathVideo = pathVideo;

    let sourceElement = this.videoElement.querySelector("source");
    sourceElement.src = this.pathVideo;

    this.videoElement.load();

  }
  enterFullScreen(videoEl) {
    if (videoEl.requestFullscreen) {
      videoEl.requestFullscreen();
    } else if (videoEl.mozRequestFullScreen) {
      /* Firefox */
      videoEl.mozRequestFullScreen();
    } else if (videoEl.webkitRequestFullscreen) {
      /* Chrome, Safari & Opera */
      videoEl.webkitRequestFullscreen();
    } else if (videoEl.msRequestFullscreen) {
      /* IE/Edge */
      videoEl.msRequestFullscreen();
    }
  }

  render() {
    this.containerElement.appendChild(this.videoElement);
    return this.containerElement;
  }

  getVideoElement() {
    return this.videoElement;
  }
}
