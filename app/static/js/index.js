import { Api } from "./api.js";
import { CreatorVideo } from "./creatorVideo.js";
import { PopUp } from "./popUp.js";

let currentVideoIndex = 0; 

function getVideoResult(result) {
  switch (result) {
    case "option_1":
      return 1;
    case "option_2":
      return 2; 
    case "default":
      return 2; 
    default:
      return 0; 
  }
}

function printResult(data) {
  if (!data || typeof data !== "object" || !data.count) {
    return `Vous avez choisi la troisième vidéo avec plus de : 50% -> par defaut`;
  }

  if (data.result === "option_1") {
    let result = calculResult(data.option_1, data.count);
    return `Vous avez choisi la première vidéo avec plus de : ${result}%`;
  } else if (data.result === "option_2") {
    let result = calculResult(data.option_2, data.count);
    return `Vous avez choisi la deuxième vidéo avec plus de : ${result}%`;
  } else if (data.result === "default") {
    return `Vous avez choisi la troisième vidéo avec plus de : 50% -> par defaut`;
  } else {
    return "Données du sondage non concluantes, chargement de la vidéo par défaut.";
  }
}

function calculResult(votes, total) {
  if (total === 0) {
    console.error(
      "Total des votes égal à 0, impossible de calculer un pourcentage."
    );
    return 50; 
  }

  let result = (votes / total) * 100;
  return result.toFixed(2); // Arrondir à 2 décimales
}

function setupVideoPlayer(videoPath) {
  const videoElement = new CreatorVideo(videoPath);
  const rootElement = document.getElementById("root");
  rootElement.innerHTML = ""; 
  rootElement.appendChild(videoElement.render());

  // Activer l'autoplay uniquement pour la deuxième vidéo
  if (currentVideoIndex === 1) {
    let videoEl = videoElement.getVideoElement();
    videoEl.muted = true; // Nécessaire pour permettre l'autoplay dans la plupart des navigateurs
    videoEl.autoplay = true;
    videoEl.play(); 
  }

  return videoElement;
}

function loadNextVideo() {
  let videoPath = videoUrls[currentVideoIndex];
  const videoElement = setupVideoPlayer(videoPath);

  videoElement.getVideoElement().addEventListener("ended", () => {
    if (currentVideoIndex === 0) {
      // Première vidéo
      handleInitialVideoEnd(videoElement);
    } else {
      handleSubsequentVideoEnd();
    }
  });
}
function handleInitialVideoEnd(videoElement) {
  const popUp = new PopUp(
    document.getElementById("popup"),
    document.querySelector(".close-btn"),
    document.querySelector("#title_popup"),
    document.querySelector("#p_popup")
  );

  popUp.initializePopupText(); 
  popUp.openPopUp(); 

  const api = new Api(api_urls);


  if (document.fullscreenElement === videoElement.getVideoElement()) {
    document.exitFullscreen();
  }

  api
    .getSheetsResponse()
    .then((response) => {
      let data = response.data;
      if (!data || typeof data !== "object") {
        console.error("Erreur de format de données.");
        popUp.displayErrorMessage();
        return;
      }

      let resultData = printResult(data);
      popUp.setCustomPopupText("Résultat du vote", resultData);

      setTimeout(() => {
        popUp.closePopUp();
        currentVideoIndex = getVideoResult(data.result);
        loadNextVideo(); 
      }, 10000); 
    })
    .catch((error) => {
      console.error("Erreur lors de la récupération des données :", error);
      popUp.displayErrorMessage();
    });
}

function handleSubsequentVideoEnd() {
  currentVideoIndex = 0;
  loadNextVideo();
}

document.addEventListener("DOMContentLoaded", () => {
  loadNextVideo(); 
});
