export class ApiCustom {
  constructor(baseUrl) {
    this.baseUrl = baseUrl + "/api/v1/";
  }

  async request(endPoint, options = {}) {
    try {
      const response = await fetch(`${this.baseUrl}${endPoint}`, options);
      if (!response.ok) {
        throw new Error(`Error: here ${response.status}`);
      }
      return await response.json();
    } catch (error) {
      throw error; 
    }
  }

  get(endPoint) {
    return this.request(endPoint);
  }

  post(endPoint, data) {
    return this.request(endPoint, {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    });
  }

  put(endPoint, data) {
    return this.request(endPoint, {
      method: "PUT",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    });
  }

  delete(endPoint) {
    return this.request(endPoint, {
      method: "DELETE",
    });
  }
}

export class Api extends ApiCustom {
  constructor(baseUrl) {
    super(baseUrl);
  }
  async getPing() {
    return await this.get("ping");
  }
  async getSheetsResponse(){
    return await this.get("sheets_start");
  }
}
