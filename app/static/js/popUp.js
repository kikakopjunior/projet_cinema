export class PopUp {
  constructor(
    popUpElement,
    btnCloseElement,
    titleElement,
    textElement,
  ) {
    this.popUpElement = popUpElement;
    this.btnCloseElement = btnCloseElement;
    this.titleElement = titleElement;
    this.textElement = textElement;

    this.setupEventListeners();
  }

  setupEventListeners() {
    this.btnCloseElement.addEventListener("click", () => {
      this.closePopUp();
    });

    this.popUpElement.addEventListener("click", (event) => {
      if (event.target === this.popUpElement) {
        this.closePopUp();
      }
    });

    document.addEventListener("keydown", (event) => {
      if (event.key === "Escape") {
        this.closePopUp();
      }
    });
  }

  openPopUp() {
    this.popUpElement.style.display = "block";
    this.autoClose(60000, 30000);
  }

  closePopUp() {
    this.popUpElement.style.display = "none";
  }

  autoClose(openDuration, closeDelay) {
    setTimeout(() => {
      this.closePopUp();
    }, openDuration + closeDelay);
  }

  initializePopupText() {
    this.textElement.textContent =
      "loading ...";
  }

  setCustomPopupText(title, text) {
    this.titleElement.textContent = title;
    this.textElement.textContent = text;
  }

  displayErrorMessage() {
    this.titleElement.textContent = "Erreur";
    this.textElement.textContent =
      "Une erreur est survenue. Pour ne pas interrompre le programme, il sera lancé par défaut.";
  }
}
